export class Node {
    /*
     * adjacent es un array de objetos
     *
     * { nodo: Nodo,
    *    peso: numero
     * }
    */
    public adjacent: Array<any>;

    constructor(public value: number, public nombre: string, public id: number) {
    }

    addAdjacent(node: any) {
        this.adjacent = node;
    }
}
