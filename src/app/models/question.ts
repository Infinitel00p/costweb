import { Node } from './node';

export class Question {
    public nodes: Array< Node >;
    constructor(public question: string, public level: Number, public image: string, public color: string) {}

    addNode(node: Node) {
        this.nodes.push(node);
    }
    addNodes(nodes: Array<Node>) {
        this.nodes = nodes;
    }

}
