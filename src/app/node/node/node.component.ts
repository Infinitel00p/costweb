import { Component, OnInit, Input, Output} from '@angular/core';
import { Node } from '../../models/node';

@Component({
  selector: 'node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.css']
})
export class NodeComponent implements OnInit {

  @Input() node: Node;
  
  constructor() { }

  ngOnInit() {
  }

}
