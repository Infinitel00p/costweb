import { Injectable } from '@angular/core';
import { Question } from '../models/question';
import { Node } from '../models/node';
import { DeprecatedI18NPipesModule } from '@angular/common';
import { tryParse } from 'selenium-webdriver/http';

@Injectable({
	providedIn: 'root'
})
export class QuestionService {
	private questions: Array<Question> = new Array<Question>();

	constructor() {

		// que web
		let n1 = new Node(150, 'Web Simple', 1);
		let n2 = new Node(300, 'Blog', 2);
		let n3 = new Node(600, 'E-Commerce', 3);
		let n4 = new Node(1500, 'Web a medida', 4);

		// usara cms
		let n5 = new Node(0, 'Si', 5);
		n5.addAdjacent([{ nodo: n1, peso: 45 }, { nodo: n2, peso: 75 }, { nodo: n3, peso: 135 }, { nodo: n4, peso: 180 }]);
		let n6 = new Node(0, 'No', 6);
		n6.addAdjacent([{ nodo: n1, peso: 75 }, { nodo: n2, peso: 105 }, { nodo: n3, peso: 165 }, { nodo: n4, peso: 240 }]);

		// tendra diseño
		let n7 = new Node(0, 'Usar plantilla', 7);
		n7.addAdjacent([{ nodo: n5, peso: 60 }, { nodo: n6, peso: 90 }]);
		let n8 = new Node(0, 'Diseño personalizado', 8);
		n8.addAdjacent([{ nodo: n5, peso: 105 }, { nodo: n6, peso: 170 }]);
		let n9 = new Node(0, 'No necesito diseño', 9);
		n9.addAdjacent([{ nodo: n5, peso: 0 }, { nodo: n6, peso: 0 }]);
		let n10 = new Node(0, 'No tengo idea', 10);
		n10.addAdjacent([{ nodo: n5, peso: 120 }, { nodo: n6, peso: 190 }]);

		// Tamaño de la web
		let n11 = new Node(0, 'Pequeña: 1-5 páginas', 11);
		n11.addAdjacent([{ nodo: n7, peso: 90 }, { nodo: n8, peso: 165 }, { nodo: n9, peso: 75 }, { nodo: n10, peso: 120 } ]);
		let n12 = new Node(0, 'Mediana: 6-10 páginas', 12);
		n12.addAdjacent([{ nodo: n7, peso: 165 }, { nodo: n8, peso: 210 }, { nodo: n9, peso: 90 }, { nodo: n10, peso: 120 } ]);
		let n13 = new Node(0, 'Mediana: más de 10 páginas', 13);
		n13.addAdjacent([{ nodo: n7, peso: 210 }, { nodo: n8, peso: 165 }, { nodo: n9, peso: 75 }, { nodo: n10, peso: 120 } ]);
		let n14 = new Node(0, 'No tengo idea', 14);
		n14.addAdjacent([{ nodo: n7, peso: 240 }, { nodo: n8, peso: 240 }, { nodo: n9, peso: 0 }, { nodo: n10, peso: 240 } ]);

		// pasarela de pagos
		let n15 = new Node(0, 'Si', 15);
		n15.addAdjacent([{ nodo: n5, peso: 45 }, { nodo: n6, peso: 90 } ]);
		let n16 = new Node(0, 'No', 16);
		n16.addAdjacent([{ nodo: n5, peso: 0 }, { nodo: n6, peso: 0 } ]);
		let n17 = new Node(0, 'No tengo idea', 17);
		n17.addAdjacent([{ nodo: n5, peso: 20 }, { nodo: n6, peso: 20 } ]);

		// integracion con api
		let n18 = new Node(0, 'Si', 18);
		n18.addAdjacent([{ nodo: n5, peso: 120 }, { nodo: n6, peso: 270 } ]);
		let n19 = new Node(0, 'No', 19);
		n19.addAdjacent([{ nodo: n5, peso: 0 }, { nodo: n6, peso: 0 } ]);
		let n20 = new Node(0, 'No tengo idea', 20);
		n20.addAdjacent([{ nodo: n5, peso: 120 }, { nodo: n6, peso: 120 } ]);

		// registro de usuarios
		let n21 = new Node(0, 'Si', 21);
		n21.addAdjacent([{ nodo: n5, peso: 90 }, { nodo: n6, peso: 240 } ]);
		let n22 = new Node(0, 'No', 22);
		n22.addAdjacent([{ nodo: n5, peso: 0 }, { nodo: n6, peso: 0 } ]);
		let n23 = new Node(0, 'No tengo idea', 23);
		n23.addAdjacent([{ nodo: n5, peso: 105 }, { nodo: n6, peso: 255 } ]);

		// web multiidioma
		let n24 = new Node(0, 'Si', 24);
		n24.addAdjacent([{ nodo: n11, peso: 30 }, { nodo: n12, peso: 45 }, { nodo: n13, peso: 90 }, { nodo: n14, peso: 105 } ]);
		let n25 = new Node(0, 'No', 25);
		n25.addAdjacent([{ nodo: n11, peso: 0 }, { nodo: n12, peso: 0 }, { nodo: n13, peso: 0 }, { nodo: n14, peso: 0 } ]);
		let n26 = new Node(0, 'No tengo idea', 26);
		n26.addAdjacent([{ nodo: n11, peso: 45 }, { nodo: n12, peso: 90 }, { nodo: n13, peso: 105 }, { nodo: n14, peso: 120 } ]);

		// preguntas
		let p1 = new Question('¿Que tipo de web necesitas?', 1, 'Pregunta 1.png', '#1bbc9b');
		p1.addNodes([n1, n2, n3, n4]);
		let p2 = new Question('¿Quieres usar algún CMS?', 2, 'Pregunta 2.png', '#0a4e7d');
		p2.addNodes([n5, n6]);
		let p3 = new Question('¿Que diseño quieres que tenga tu web?', 3, 'Pregunta 3.png', '#f6871c');
		p3.addNodes([n7, n8, n9, n10]);
		let p4 = new Question('¿Que tan grande será tú web?', 4, 'pregunta 4.png', '#08c6a2');
		p4.addNodes([n11, n12, n13, n14]);
		let p5 = new Question('¿Se harán pagos desde tu web?', 5, 'pregunta 5.png', '#d8472a');
		p5.addNodes([n15, n16, n17]);
		let p6 = new Question('¿Tú web estará integrada con un API?', 6, 'pregunta 6.png', '#266590');
		p6.addNodes([n18, n19, n20]);
		let p7 = new Question('¿Tú web tendrá registro de usuarios?', 7, 'pregunta 7.png', '#8e4386');
		p7.addNodes([n21, n22, n23]);
		let p8 = new Question('¿Tú web será multiidioma?', 8, 'pregunta 8.png', '#19b5db');
		p8.addNodes([n24, n25, n26]);

		this.questions.push(p1);
		this.questions.push(p2);
		this.questions.push(p3);
		this.questions.push(p4);
		this.questions.push(p5);
		this.questions.push(p6);
		this.questions.push(p7);
		this.questions.push(p8);
	}

	/**
	 * getQuestion
	 * se obtiene la pregunta por su indice
	 */
	public getQuestionIndex(index): Question {
		this.constructor();
		return this.questions[index];
	}

	/**
	 * getIndex
	 * develve el tamaño del arreglo
	 */
	public getIndex(): number {
		return this.questions.length;
	}
}

