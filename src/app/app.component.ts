import { Component } from '@angular/core';
import { QuestionService } from './services/question.service';
import { Question } from './models/question';
import { Node } from './models/node';

import { trigger, style, transition, animate, state , keyframes} from '@angular/animations';

/*animate("5s", keyframes([
	style({ backgroundColor: "red" }) // offset = 0
	style({ backgroundColor: "blue" }) // offset = 0.33
	style({ backgroundColor: "orange" }) // offset = 0.66
	style({ backgroundColor: "black" }) // offset = 1
  ]))
*/

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
      	trigger('enter', [

			state('void', style({
				opacity: 0
			})),
          	state(':enter', style({
            	opacity: 1
		  	})),
          	state(':leave', style({
            	opacity: 0
			})),
			transition(':enter', animate('500ms', keyframes([
                style({ opacity: 0, transform: 'translateX(-2000px)' }), // offset = 0
                style({ opacity: 0.33, transform: 'translateX(-1333px)' }), // offset = 0.33
                style({ opacity: 0.66, transform: 'translateX(-666px)' }), // offset = 0.66
                style({ opacity: 1, transform: 'translateX(000px)' }), // offset = 1
              ]))),
              transition(':leave', animate('200ms', keyframes([
                style({ opacity: 1, transform: 'translateX(000px)' }), // offset = 0
                style({ opacity: 0.66, transform: 'translateX(666px)' }), // offset = 0.33
                style({ opacity: 0.33, transform: 'translateX(1333px)' }), // offset = 0.66
                style({ opacity: 0, transform: 'translateX(2000px)' }), // offset = 1
              ]))),
			//transition('in => out', animate('500ms ease-out')),

		]),
		trigger('bounce', [

			state('void', style({
				opacity: 0
			})),
			state(':enter', style({
				opacity: 1
			})),
			state(':leave', style({
				opacity: 0
		  	})),
			transition('* => *', animate('500ms ease-in')),
			//transition('in => out', animate('500ms ease-out')),

	  	]),
/*
	  trigger('bounce', [
		state('void,', style({
			opacity: 0
		})),
		transition(':enter', [
			animate (500, style({
				opacity: 1
			}))
		])

	])*/
  ]
})
export class AppComponent {
    title = 'Cuanto-Cuestas';
    cost = 0;
    answered: Array<Question> = new Array<Question>();
    currentQuestion: Question;
    currentIndex: number = 0;
    start: boolean = false;
    final: boolean = false;
    seeAnswers: boolean = false;
    edit = false;
	color = '#266590';
	questionTrigger: string;

    constructor(public _questionsService: QuestionService) {
		this.questionTrigger = 'out';
	}

    ngOnInit() {
    }

    init () {
        this.start = true;
        this.currentQuestion = this._questionsService.getQuestionIndex(0);
		this.color = this.currentQuestion.color;

    }

    editAllAnswer() {
        let cost = 0;

        for (let i = 0; i < this.answered.length; i++) {
            const node = this.answered[i].nodes[0];
            if (node.value) { cost += node.value; } else {
                for (let j = 0; j < node.adjacent.length; j++) {
                    for (let k = 0; k < i; k++) {
                        if (this.answered[k].nodes[0].id === node.adjacent[j].nodo.id) {
                            cost += node.adjacent[j].peso;
                            break;
                        }
                    }
                }
            }
        }

        this.cost = cost;
    }

    addAnswered(node: Node) {


        if (this.edit) {

            this.answered[this.currentIndex].nodes[0] = node;
            this.editAllAnswer();

            this.edit = false;
        } else {

            const q = this.currentQuestion;
            q.nodes = [node];
            this.answered.push(q);
            // this.calcular(node, this.answered.length - 2);
            this.calcular(node, false);

            this.currentIndex ++;
            if (this.answered.length === this._questionsService.getIndex()) {
                this.final = true;
				this.color = '#266590';

            } else {
                this.currentQuestion = this._questionsService.getQuestionIndex(this.answered.length);
                this.color = this.currentQuestion.color;
            }
		}

		this.questionTrigger = 'out';
		this.questionTrigger = 'in';


    }

    calcular(node: Node, restar) {
        if (! restar) {
            if (node.value) { this.cost += node.value; } else {
                for (let i = 0; i < node.adjacent.length; i++) {
                    for (let j = 0; j < this.answered.length; j++) {
                        if (this.answered[j].nodes[0].id === node.adjacent[i].nodo.id) {
                            this.cost += node.adjacent[i].peso;
                            return;
                        }
                    }
                }
            }
        } else {
            if (node.value) { this.cost -= node.value; } else {
                for (let i = 0; i < node.adjacent.length; i++) {
                    for (let j = 0; j < this.answered.length; j++) {
                        if (this.answered[j].nodes[0].id === node.adjacent[i].nodo.id) {
                            this.cost -= node.adjacent[i].peso;
                            return;
                        }
                    }
                }
            }
        }

    }

    editAnswered(i) {
        this.edit = true;
        this.currentQuestion = this._questionsService.getQuestionIndex(i);
		this.currentIndex = i;

		this.color = this.currentQuestion.color;
    }

    previous() {
        this.currentQuestion = this._questionsService.getQuestionIndex(this.currentIndex - 1);
        this.currentIndex --;
        this.calcular(this.answered[this.currentIndex].nodes[0], true);
		this.answered.splice(this.currentIndex);
		this.color = this.currentQuestion.color;
    }

    backToInit () {
        this.cost = 0;
        this.answered= new Array<Question>();
        this.currentQuestion = null;
        this.currentIndex = 0;
        this.start = false;
        this.final = false;
        this.seeAnswers = false;
		this.edit = false;

		this.color = '#266590';
    }

    seeAmswuers() {
        this.seeAnswers = true;
    }
}
